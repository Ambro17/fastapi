"""Esta API servirá para introducir los conceptos de APIs REST

Ella soportará:
1. Leer parametros por get
2. Leer parametros por post (json)
3. Leer headers del request
4. Responder un json seteando el tipo del contenido (Content-Type)
"""
from flask import Flask, request
from flask.helpers import make_response


app = Flask(__name__)


@app.route('/')
def saludar():
    """Saluda al mundo, o al nombre que sea pasado por parámetro.

    Ejemplos de uso:
        http get http://127.0.0.1:5000
        Hola, Mundo!

        http get http://127.0.0.1:5000?nombre=Nahuel
        Hola, Nahuel!
    """
    user = request.args.get('nombre', 'Mundo')
    return f'Hola, {user}!'


personas = []


@app.route('/add_person', methods=['POST'])
def agregar_persona():
    """Agrega una persona a la lista de personas a saludar

    Ejemplos de uso:
        http post http://127.0.0.1:5000/add_person
    """
    argumentos = request.get_json()
    if not argumentos:
        content_type = request.content_type
        return (
            f'El POST debe establecer "application/json" como Content-Type, pero recibí "{content_type}"'
        )

    persona = argumentos.get('persona')
    if not persona:
        return make_response('Debo recibir el argumento persona', 400)

    personas.append(persona)
    return make_response({
        'personas': personas
    }, 201)


@app.route('/mostrarheaders', methods=['GET'])
def leer_headers():
    """Lee los headers del request recibidos y los devuelve en formato json

    Ejemplos de uso:
        http get http://127.0.0.1:5000/mostrarheaders Header1:Hola Header2:Chau
    """
    response = {
        key: value
        for key, value in request.headers.items()  # pylint: disable=R1721
    }

    return make_response(
        response,
        200,  # HTTP Status Code
        {'Content-Type': 'application/json'}  # Response Headers
    )
