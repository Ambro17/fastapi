from application.api import app
import json


def test_inicio():
    response = app.test_client().get('/')

    assert response.status_code == 200
    assert response.data == b'Hola, Mundo!'


def test_post_personas():
    response = app.test_client().post(
        '/add_person',
        data=json.dumps({'persona': 'Nahuel'}),
        content_type='application/json',
    )

    assert response.status_code == 201
    assert response.json == {'personas': ['Nahuel']}


def test_leer_headers():
    # Completar
    pass
