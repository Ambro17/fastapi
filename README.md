# RestApi

Proyecto de ejemplo para aprender sobre APIs REST, HTTP, y serialización JSON.

## Prerequisitos
Python3.6 o superior

[httpie](https://httpie.io/) 
(Se puede instalar con `sudo apt install httpie`) para hacer requests a nuestra api

## Comenzando
1. Crear un entorno virtual aislado (`virtualenv`) para nuestro proyecto 
```
python3.7 -m venv env
# Activar el entorno virtual para entrar a 'tierra segura'
source env/bin/activate
```
2. Instalar las dependencias del proyecto en el entorno aislado.
```
pip install -r requirements.txt
```
3. Iniciar nuestra api
```
./start.sh
```

## Funcionalidades
1. Leer parametros por get
2. Leer parametros por post (json)
3. Leer headers del request
4. Responder un json seteando el content type


## Conceptos
- API
- REST
- HTTP
- GET/POST
- JSON y formatos de serialización


## Chequeo de Calidad
Luego de introducir algún cambio, podés asegurarte que todo siga andando corriendo
los chequeos de calidad
```bash
./build.sh
```
